//! Dependencies
const bcrypt = require('bcryptjs');

//! Modules
const User = require('../models/User');
const auth = require('../auth');

let registerUser = (req, res) => {
  //* Destructuring req.body
  const { name, email, password } = req.body;

  User.find({ email })
    .then((result) => {
      if (result.length === 0) {
        const hashPassword = bcrypt.hashSync(password, 10);
        const newUser = new User({ name, email, password: hashPassword });

        newUser
          .save()
          .then((result) => res.send(result))
          .catch((err) => res.send(err));
      } else {
        res.send('Email already exists!');
      }
    })
    .catch((err) => res.send(err));
};

//TODO: Finish comparison and return generated token
// const loginUser = (req, res) => {
//   const { email, password } = req.body;

//   User.findOne({ email })
//     .then((result) => {
//       let hashPassword = bcrypt.compareSync(password, result.password);

//       if (hashPassword) {
//         return res.send({ accessToken: auth.generateToken(result) });
//       } else {
//         res.send('Incorrect credentials, please login');
//       }
//     })
//     .catch((err) => res.send(err));
// };
let getAllUsers = (req, res) => {
  User.find({})
    .then((result) => res.send(result))
    .catch((err) => res.send(err));
};

let loginUser = (req, res) => {
  User.findOne({ email: req.body.email })
    .then((foundUser) => {
      if (foundUser === null) {
        return res.send('No user found in the database');
      } else {
        const isPasswordCorrect = bcrypt.compareSync(
          req.body.password,
          foundUser.password
        );

        if (isPasswordCorrect) {
          return res.send({ accessToken: auth.generateToken(foundUser) });
        } else {
          return res.send('Incorrect password, please try agan');
        }
      }
    })
    .catch((err) => res.send(err));
};

let upateAdmin = (req, res) => {
  console.log(req.user.id); // id of the logged in user

  console.log(req.params.id); //id of the user we want to update

  let updates = {
    isAdmin: true,
  };

  User.findByIdAndUpdate(req.params.id, updates, { new: true })
    .then((updateUser) => res.send(updateUser))
    .catch((err) => res.send(err));
};

module.exports = {
  registerUser,
  loginUser,
  upateAdmin,
  getAllUsers,
};
