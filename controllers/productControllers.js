const Product = require('../models/Product');

const createProduct = (req, res) => {
  const { name, categories, description, price } = req.body;

  //TODO: Check if object is a duplicate, e.g. name, categories, description and price are all the same
  const newProduct = new Product({
    name,
    categories,
    description,
    price,
  });

  newProduct
    .save()
    .then((result) => res.send(result))
    .catch((err) => res.send(err));
};

const findAllProduct = (req, res) => {
  Product.find({})
    .then((result) => res.send(result))
    .catch((err) => res.send(err));
};

const findProductByID = (req, res) => {
  Product.findOne({ _id: req.params.id })
    .then((result) => res.send(result))
    .catch((err) => res.send(err));
};

const findProductByName = (req, res) => {
  Product.find({ name: req.body.name })
    .then((result) => res.send(result))
    .catch((err) => res.send(err));
};

/* 
let updateCourse = (req, res) => {
  let updates = {
    name: req.body.name,
    description: req.body.description,
    price: req.body.price,
  };

  Course.findByIdAndUpdate(req.params.id, updates, { new: true })
    .then((upCourse) => res.send(upCourse))
    .catch((error) => res.send(error));
};
*/
const editProduct = (req, res) => {
  let updateItem = {
    name: req.body.name,
    categories: req.body.categories,
    description: req.body.description,
    price: req.body.price,
  };

  Product.findByIdAndUpdate(req.params.id, updateItem, { new: true })
    .then((result) => res.send(result))
    .catch((error) => res.send(error));
};

module.exports = {
  createProduct,
  findAllProduct,
  findProductByID,
  findProductByName,
  editProduct,
};
