const express = require('express');
const router = express.Router();

//! Modules
const userControllers = require('../controllers/userControllers');
const auth = require('../auth');
const { verify, verifyAdmin } = auth;

router.post('/registerUser', userControllers.registerUser);
router.get('/login', userControllers.loginUser);
router.put(
  '/updateUserAdmin/:id',
  verify,
  verifyAdmin,
  userControllers.upateAdmin
);

router.get('/', userControllers.getAllUsers);

module.exports = router;
