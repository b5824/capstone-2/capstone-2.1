const express = require('express');
const router = express.Router();
const auth = require('../auth');

const { verify, verifyAdmin } = auth;

const productControllers = require('../controllers/productControllers');

//TODO: Add auth to verify if user is admin
router.post(
  '/addProduct',
  verify,
  verifyAdmin,
  productControllers.createProduct
);

//TODO: Add auth to verify if user is logged in but not necessarily an admin
router.get('/', productControllers.findAllProduct);

router.get('/find/:id', productControllers.findProductByID);
router.get('/find', productControllers.findProductByName);

router.put('/update/:id', verify, verifyAdmin, productControllers.editProduct);

module.exports = router;
