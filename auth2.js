//dependencies
const jwt = require('jsonwebtoken');
const secret =
  ' bd5a041753753d67927e77fad8767b16fdaf9f6ae872f8296f69903228932c3795a32f324987432cf14fa063400f14b2e04e5b020924c0b54d108e7e64369519';

// modules

//jwt creation
const generateToken = (user) => {
  const { name, email } = user;
  const data = { name, email };
  const token = jwt.sign(data, secret, {});
  return token;
};

//jwt verification
const verifyToken = (req, res) => {
  //get auth header value
  let bearerHeader = req.headers['authorization'];
  const token = bearerHeader.split(' ')[1];
  //check if bearer is undefined

  jwt.verify(token, secret, (err, decodedPayload) => {
    if (typeof bearerHeader === 'undefined') {
      //forbidden
      res.send({
        auth: 'Failed',
        message: err.message,
      });
    } else {
      req.user = decodedPayload;
    }
  });
};

module.exports = {
  generateToken,
  verifyToken,
};

//admin verification
