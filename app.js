//! Requiring the modules
const express = require('express');
const mongoose = require('mongoose');

//! Port
const port = 4000;

//! Creating an instance of the server
const app = express();

//! Middleware
app.use(express.json());

//! Database connection (URI, Options)
mongoose.connect(
  'mongodb+srv://admin:admin@wdc028-course-booking.j89li.mongodb.net/Capstone2?retryWrites=true&w=majority',
  {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  }
);

//! Optional - Message to inform programmer if DB is connected or have an error
let db = mongoose.connection;
db.on('error', console.error.bind(console, 'Connection error'));
db.once('open', () => console.log('Successfully connected to MongoDB'));

//! Group routing
const userRoutes = require('./routes/userRoutes');
app.use('/users', userRoutes);

const productRoutes = require('./routes/productRoutes');
app.use('/products', productRoutes);

//! Port listener
app.listen(port, () => console.log(`Listening to port: ${port}`));
