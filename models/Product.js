const mongoose = require('mongoose');
//https://mongoosejs.com/docs/validation.html
const productSchema = new mongoose.Schema({
  name: {
    type: String,
    required: [true, 'Product name cannot be empty'],
  },
  categories: {
    type: String,
    enum: {
      values: [
        'Home, Garden, Pets & DIY',
        'Toys, Children & Baby',
        'Clothes, Shoes, Jewellery & Accessories',
        'Sports & Outdoors',
        'Food & Grocery',
      ],
      message: '{VALUE} is not supported',
    },
  },
  description: {
    type: String,
    required: [true, 'Product name cannot be empty'],
  },
  price: {
    type: Number,
    required: [true, 'Product name cannot be empty'],
  },
  isActive: {
    type: Boolean,
    default: true,
  },
  createdOn: {
    type: Date,
    default: new Date(),
  },
});

module.exports = mongoose.model('Product', productSchema);
